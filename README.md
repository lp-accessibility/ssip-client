Rust SSIP Client
================

[![build status](https://gitlab.com/lp-accessibility/ssip-client/badges/main/pipeline.svg)](https://gitlab.com/lp-accessibility/ssip-client/commits/main)
[![Crates.io Version](https://img.shields.io/crates/v/ssip-client.svg)](https://crates.io/crates/ssip-client)
[![docs.rs](https://docs.rs/ssip-client/badge.svg)](https://docs.rs/ssip-client/latest/ssip_client/)
![Crates.io License](https://img.shields.io/crates/l/ssip-client)

Speech Dispatcher [SSIP client library](http://htmlpreview.github.io/?https://github.com/brailcom/speechd/blob/master/doc/ssip.html) in pure rust.

The API is synchronous by default.

A non-blocking API can be used with a low-level polling mechanism based on `poll`, or
with [mio](https://crates.io/crates/mio).

- [x] Unix socket.
- [x] TCP socket.
- [x] Stop, cancel, pause and resume.
- [x] List, set voices.
- [x] Set rate, pitch, volume.
- [x] Notifications.
- [x] Message history.

The crate is considered as feature complete. It is in maintenance mode. For a real async API, see
[ssip-client-async](https://crates.io/crates/ssip-client-async) which is a fork of this crate.

Getting Started
---------------

To use the synchronous API or an asynchronous API compatible with low-level crates based on `poll`, use:

```toml
[dependencies]
ssip-client = "0.10"
```

For the asynchronous API, use:
```toml
[dependencies]
ssip-client = { version = "0.10", features = ["async-mio"] }
```

Example
-------

```rust
use ssip_client::{FifoBuilder, ClientName};
let mut client = fifo::Builder::new().build()?;
client
    .set_client_name(ClientName::new("joe", "hello"))?
    .check_client_name_set()?;
let msg_id = client.speak()?.send_line("hello")?.receive_message_id()?;
client.quit()?;
```

See [other examples](https://gitlab.com/lp-accessibility/ssip-client/-/tree/main/examples) in the repository.

License
-------

This software is distributed under the terms of both the MIT license
and the Apache License (Version 2.0).

See [LICENSE-APACHE](LICENSE-APACHE) and [LICENSE-MIT](LICENSE-MIT) for details.
